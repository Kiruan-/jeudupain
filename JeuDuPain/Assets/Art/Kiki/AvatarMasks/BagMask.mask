%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BagMask
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/IKRadio
    m_Weight: 0
  - m_Path: Armature/Pelvis
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_l/Tibia_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_l/Tibia_l/Foot_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_r/Tibia_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Leg_r/Tibia_r/Foot_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/skirtfront
    m_Weight: 0
  - m_Path: Armature/Pelvis/skirtfront_001
    m_Weight: 0
  - m_Path: Armature/Pelvis/skirtside
    m_Weight: 0
  - m_Path: Armature/Pelvis/skirtside_001
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/bow1
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/bow2
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/fringe1
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/fringe2
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/Hair1
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/Hair2
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/hair3
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Neck/Head_0/hair4
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/broom
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/fingers_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/fingers_l/fingertips_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/index_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/index_l/indextip_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/radiotop
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/radiotop/radiobottom
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/thumb_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_l/Arm_l/Forearm_l/Hand_l/BroomTarget/thumb_l/thumbtip_l
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/fingers_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/fingers_r/fingertips_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/index_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/index_r/indextip_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/thumb_r
    m_Weight: 0
  - m_Path: Armature/Pelvis/Torso/Shoulder_r/Arm_r/Forearm_r/Hand_r/thumb_r/thumbtip_r
    m_Weight: 0
  - m_Path: Armature/TargetHand_l
    m_Weight: 0
  - m_Path: Armature/TargetHand_r
    m_Weight: 0
  - m_Path: Armature/TargetLeg_l
    m_Weight: 0
  - m_Path: Armature/TargetLeg_r
    m_Weight: 0
  - m_Path: Aspi
    m_Weight: 0
  - m_Path: BalaiBrosse
    m_Weight: 0
  - m_Path: BalaiSimple
    m_Weight: 0
  - m_Path: BlackDress
    m_Weight: 0
  - m_Path: Bow
    m_Weight: 0
  - m_Path: ClothesNausicaa1
    m_Weight: 0
  - m_Path: HairBase
    m_Weight: 0
  - m_Path: HairCheetah
    m_Weight: 0
  - m_Path: HairChihiro
    m_Weight: 0
  - m_Path: HairKiki
    m_Weight: 0
  - m_Path: HairMononoke
    m_Weight: 0
  - m_Path: HairPigtails
    m_Weight: 0
  - m_Path: HairSophie
    m_Weight: 0
  - m_Path: HatNausicaa1
    m_Weight: 0
  - m_Path: HatPorcoRosso
    m_Weight: 0
  - m_Path: Head
    m_Weight: 0
  - m_Path: NausicaaMask
    m_Weight: 0
  - m_Path: Sac
    m_Weight: 1
