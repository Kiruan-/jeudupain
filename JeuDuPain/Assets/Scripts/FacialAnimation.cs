﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacialAnimation : MonoBehaviour
{
    Animator animator;
    float active = 1f;
    float inactive = 0f;

    public float timeBetweenBlinks = 2f;
    public float blinkTime = .08f;
    private float initialBlinkTime;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetFloat("Neutral", active);

        initialBlinkTime = timeBetweenBlinks;
    }

    void Update()
    {
        Blink();
    }

    void Blink()
    {
        timeBetweenBlinks -= Time.deltaTime;

        if (timeBetweenBlinks <= 0f)
        {
            animator.SetFloat("EyesClosed", active);

            StartCoroutine(ClosedEyes());
        }
    }

    private IEnumerator ClosedEyes()
    {
        yield return new WaitForSeconds(blinkTime);

        animator.SetFloat("EyesClosed", inactive);

        timeBetweenBlinks = initialBlinkTime - (Random.Range(-1f, 1f));
    }
}
