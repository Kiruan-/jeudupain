﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    private const float secondesIRLparTour = 60f;    //mettre 1440f pour 1 seconde irl = 1 minute de jeu

    private Transform clockHourHandTransform;   //aiguille des heures
    private Transform clockMinuteHandTransform; //aiguille des minutes
    private Text timeText;  //pour afficher l'heure en digital

    private float tourComplet = 0.2922021f; //valeur de départ pour démarrer à 7h, pas touche :p

    //variables qui contiennent l'heure qu'il est
    public float hours;
    public float minutes;

    public bool finDeJournee = false;

    private void Awake()
    {
        clockHourHandTransform = transform.Find("hourHand");
        clockMinuteHandTransform = transform.Find("minuteHand");
        timeText = transform.Find("timeText").GetComponent<Text>();
    }

    private void Update()
    {
        if (hours < 22f)
        {
            tourComplet += Time.deltaTime / secondesIRLparTour;

            float tourNormalized = tourComplet % 1f;

            float rotationDegreesPerDay = 360f;
            float hoursPerDay = 24f;
            float minutesPerHour = 60f;

            //rotation des aiguilles
            clockHourHandTransform.eulerAngles = new Vector3(0, 0, -tourNormalized * rotationDegreesPerDay * 2f);
            clockMinuteHandTransform.eulerAngles = new Vector3(0, 0, -tourNormalized * rotationDegreesPerDay * 2f * hoursPerDay);

            //calcul de l'heure qu'il est
            hours = Mathf.Floor(tourNormalized * hoursPerDay);
            minutes = Mathf.Floor(((tourNormalized * hoursPerDay) % 1f) * minutesPerHour);

            //strings pour afficher l'heure en chiffres
            string hoursString = hours.ToString("00");
            string minutesString = minutes.ToString("00");
            timeText.text = hoursString + ":" + minutesString;

        } else {
            if (!finDeJournee)
            {
                Debug.Log("journée terminée !");
                finDeJournee = true;
            }
        }
    }
}
